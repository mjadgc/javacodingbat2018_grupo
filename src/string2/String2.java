/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package string2;

/**
 *
 * @author Marta Márquez Olalla
 */
public class String2 {
    /*Ejercicio  doubleChar de la página CodingBat. El primero de los ejercicios de los String2.
    Enunciado; Given a string, return a string where for every char in the original, there are two chars.*/
    public String doubleChar(String str) {
  String aux="";
  
  for(int i=0;i<str.length();i++){
    aux+=str.charAt(i);
    aux+=str.charAt(i);
  }
  return aux;
}
/*Ejercicio countCode de la página de CodingBat.
    Return the number of times that the string "code" appears anywhere in the given string, 
    except we'll accept any letter for the 'd', so "cope" and "cooe" count.*/
    public int countCode(String str) {
  int auxiliar=0;
  
  for(int i=0; i<str.length(); i++){
    if(str.length()>3 && i+3<str.length()
    && str.substring(i,i+1).equals("c") && 
    str.substring(i+1,i+2).equals("o") &&
    str.substring(i+3,i+4).equals("e")){
      auxiliar++;
    }
  }
  return auxiliar;
}
/*Ejercicio bobThere de la página CodingBat.
    Return true if the given string contains a "bob" string, but where the middle 'o' char can be any char.*/
    
    public boolean bobThere(String str) {
  
  for(int i=0; i<str.length();i++){
    if(str.length()>2 && i<str.length()-2 &&
      str.substring(i,i+1).equals("b") && 
    str.substring(i+2,i+3).equals("b")){
      return true;
    }
  }
  return false;
}
/*Ejercicio countHi de la página CodingBat.
    Return the number of times that the string "hi" appears anywhere in the given string.*/
    
    public int countHi(String str) {
  int auxiliar=0;
  
  for(int i=0; i<str.length();i++){
    if(i<str.length()-1 && str.substring(i,i+2).equals("hi")){
      auxiliar++;
    }
  }
  return auxiliar;
}
  
    /*Ejercicio repeatEnd de la página CodingBat.
    Given a string and an int n, return a string made of n repetitions of the last n characters of the string.
    You may assume that n is between 0 and the length of the string, inclusive.*/
    
    public String repeatEnd(String str, int n) {
  String auxiliar=str.substring(str.length()-n);
  String aux="";
  
  for(int i=0;i<n; i++){
    aux=aux+auxiliar;
  }
  
  return aux;
}
/*Ejercicio repeatFront de la página CodingBat.
    
Given a string and an int n, return a string made of the first n characters of the string, 
    followed by the first n-1 characters of the string, and so on. You may assume that n is between 0
    and the length of the string, inclusive (i.e. n >= 0 and n <= str.length()).*/
    
    public String repeatFront(String str, int n) {
  String aux="";
  
  for(int i=0; i<n;i++){
    aux+=str.substring(0,n-i);
  }
  
  return aux;
}
/*Ejercicio endOther de la página CodingBat.
    Given two strings, return true if either of the strings appears at the very end of the other string,
    ignoring upper/lower case differences (in other words, the computation should not be "case sensitive").
    Note: str.toLowerCase() returns the lowercase version of a string.*/
    
    public boolean endOther(String a, String b) {
  String auxiliar=a.toLowerCase();
  String aux=b.toLowerCase();
  String len=a+b;
  String fin1="";
  String fin2="";
  
  for(int i=0;i<len.length();i++){
    if(a.length()>b.length()){
      fin1=auxiliar.substring(a.length()-b.length());
    }
    if(a.length()<b.length()){
      fin2=aux.substring(b.length()-a.length());
    }
  }
  
  if(fin1.equals(aux) || fin2.equals(auxiliar) || auxiliar.equals(aux)){
    return true;
  }else{
    return false;
  }
}

    /*Ejercicio xyBalance de la página CodingBat.
    We'll say that a String is xy-balanced if for all the 'x' chars in the string, there exists a 'y'
    char somewhere later in the string. So "xxy" is balanced, but "xyx" is not. One 'y' can balance
    multiple 'x's. Return true if the given string is xy-balanced.
*/
    
    public boolean xyBalance(String str) {
  int auxiliar=0;
  
  for(int i=0;i<str.length(); i++){
    if(str.charAt(str.length()-1)=='x'){
      return false;
    }
    if(str.charAt(i)== 'x'){
      auxiliar++;
    }
    if(auxiliar>0 && str.charAt(i)=='y'){
      return true;
    }
    
  }
  
  if(str.length()==0 || str.length()==1 && str.charAt(0)=='y' || !str.contains("x")){
    return true;
  }
  return false;
  
}
/*Ejercicio catdog de CodingBat.
    Return true if the string "cat" and "dog" appear the same number of times in the given string.*/
    
    public boolean catDog(String str) {
  int cat=0;
  int dog=0;
  
  for(int i=0;i<str.length();i++){
    if(i+3<=str.length() && str.substring(i,i+3).equals("cat")){
      cat++;
    }
    if(i+3<=str.length() && str.substring(i,i+3).equals("dog")){
      dog++;
    }
    
  }
  if(cat==dog){
    return true;
  }else{
    return false;
  }
}

/*Ejercicio mixString de CodingBat.
    Given two strings, a and b, create a bigger string made of the first char of a,
    the first char of b, the second char of a, the second char of b, and so on.
    Any leftover chars go at the end of the result.*/
    
    public String mixString(String a, String b) {
  String auxiliar="";
  String len=a+b;
  
  for(int i=0;i<len.length();i++){
    if(i<a.length()){
    auxiliar+=a.substring(i,i+1);
    }
    if(i<b.length()){
    auxiliar+=b.substring(i,i+1);
    }
  }
  return auxiliar;
}

    /*Ejercicio repeatSeparator de CodingBat.
    Given two strings, word and a separator sep, return a big string made of 
    count occurrences of the word, separated by the separator string.*/
    
    public String repeatSeparator(String word, String sep, int count) {
  String auxiliar="";
  
  for(int i=0;i<count;i++){
    auxiliar+=word;
    if(i<count-1){
    auxiliar+=sep;
    }
  }
  
  return auxiliar;
}

    /*Ejercicio xyzTHere de CodingBat.
    Return true if the given string contains an appearance of "xyz" where the xyz
    is not directly preceeded by a period (.). So "xxyz" counts but "x.xyz" does not.*/
    
    public boolean xyzThere(String str) {
  
  for(int i=0; i<str.length(); i++){
    
    if(i+3<=str.length() && str.substring(i,i+3).equals("xyz")){
       if(i-1>=0 && str.charAt(i-1)!='.'){
          return true;
    }
    if(str.substring(0,3).equals("xyz")){
      return true;
    }
    
  }
  }
  return false;
 
}

    
}
